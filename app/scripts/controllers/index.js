'use strict';

/**
 * @ngdoc function
 * @name bookingAppFrontendApp.controller:IndexCtrl
 * @description
 * # IndexCtrl
 * Controller of the bookingAppFrontendApp
 */
angular.module('bookingAppFrontendApp')
  .controller('IndexCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
