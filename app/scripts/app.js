'use strict';

/**
 * @ngdoc overview
 * @name bookingAppFrontendApp
 * @description
 * # bookingAppFrontendApp
 *
 * Main module of the application.
 */
var app = angular.module('bookingAppFrontendApp', [
    'ui.router',
    'ui.bootstrap',
    'ui.bootstrap.tpls',
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'restangular',
    'satellizer',
    //'ui.select',
    'rx'
  ]);
  

app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/index');

    $stateProvider.state('main', {
        url: '/',
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        params: {
            redirectTo: null
        },
        title: 'Booking App'
    });

    $stateProvider.state('main.signin', {
        url: '^/signin',
        templateUrl: 'views/signin.html',
        controller: 'SigninCtrl',
        params: {
            redirectTo: null
        },
        title: 'SignIn'
    });
    
    $stateProvider.state('main.signup', {
        url: '^/signup',
        templateUrl: 'views/signup.html',
        controller: 'SignupCtrl',
        params: {
            redirectTo: null
        },
        title: 'SignIn'
    });
    
    $stateProvider.state('main.index', {
        url: '^/index',
        templateUrl: 'views/index.html',
        controller: 'IndexCtrl',
        params: {
            redirectTo: null
        },
        title: 'Booking App'
    });
    
}]);

app.constant('ENV', {
    name:'dev',
    apiEndpoint:'http://localhost:9000/api'
});

app.config(function ($httpProvider, $provide, ENV, RestangularProvider, $authProvider) {
    RestangularProvider.setBaseUrl(ENV.apiEndpoint);
    //RestangularProvider.setDefaultHeaders({'Access-Control-Allow-Origin': 'false'});
    $authProvider.httpInterceptor = false; 
});

app.config(['$authProvider', 'ENV', function ($authProvider, ENV) {

    var redirectUri = window.location.origin || window.location.protocol + '//' + window.location.host;
    redirectUri += '/signin';

    // Auth config
    $authProvider.httpInterceptor = true; // Add Authorization header to HTTP request
    $authProvider.loginOnSignup = true;
    $authProvider.loginRedirect = null;
    $authProvider.logoutRedirect = '/';
    $authProvider.loginUrl = ENV.apiEndpoint+'/authenticate/signIn';
    $authProvider.loginRoute = ENV.apiEndpoint+'/authenticate/signIn';
    $authProvider.signupUrl = ENV.apiEndpoint+'/authenticate/signUp';
    $authProvider.signupRoute = ENV.apiEndpoint+'/authenticate/signUp';
    $authProvider.tokenName = 'token';
    $authProvider.tokenPrefix = 'satellizer'; // Local Storage name prefix
    $authProvider.authHeader = 'X-Auth-Token';
    $authProvider.tokenHeader = 'X-Auth-Token';
    $authProvider.tokenType = null;
    $authProvider.signupRedirect = null;
}]);

app.run(function($rootScope, $auth, $state, $location){

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
        var requiresLogin = toState.data && toState.data.requiresLogin;
        var isAuthenticated = $auth.isAuthenticated();
        if (requiresLogin && !isAuthenticated) {
            event.preventDefault();
            $state.go('main.signin', { redirectTo: $location.url() });
        } else if(toState.name === 'main') {
            event.preventDefault();
            $state.go('main.index');
        }
    });

});
