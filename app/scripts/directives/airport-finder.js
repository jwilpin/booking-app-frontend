'use strict';

/**
 * @ngdoc directive
 * @name bookingAppFrontendApp.directive:airportFinder
 * @description
 * # airportFinder
 */
angular.module('bookingAppFrontendApp').directive('airportFinder', ['AirportService', 'observeOnScope', function (AirportService, observeOnScope) {
    return {
      templateUrl: 'views/airport-finder.html',
      restrict: 'E',
      scope: {
      	model: '=',
	    name: '@',
	    required: '=',
	    disabledField: '=',
	    placeholder: '@'
      },
      link: function postLink($scope, element, attrs) {
        
      	$scope.query = "";

        $scope.airport = {
			selected: null
	    };

	    $scope.airports = [];

 		var setAirport = function () {
    		var airportSelected = $scope.airports.filter(function (airport) {
    		    return airport.apid === $scope.model.apid;
    		});
    		if(airportSelected.size() > 0){
    			$scope.model = airportSelected[0];
    		}
	    };

	    var loadAirports = function(){
		    $scope.loading = true;
		    AirportService.getAirports(query).then(function (airports) {
	      		$scope.loading = false;
	      		$scope.airports = airports;
	      		if($scope.model) {
	      		    setAirport();
	      		}
	      	});
		};

	    observeOnScope($scope, 'query')
      	.subscribe(function(change) {
        	$scope.query = change.newValue;
          	loadAirports();
      	});

      }
    };
  }]);
