'use strict';

/**
 * @ngdoc directive
 * @name bookingAppFrontendApp.directive:sessionHeader
 * @description
 * # sessionHeader
 */
angular.module('bookingAppFrontendApp').directive('sessionHeader', ['UserService', function (UserService) {
    return {
      templateUrl: 'views/header.html',
      restrict: 'E',
      link: function postLink($scope, element, attrs) {
      	
      	UserService.authenticatedObservable.subscribe(function (authenticated) {
	      console.log("user authenticated status change: ", authenticated);
	      $scope.authenticated = authenticated;
	      if(authenticated) {
	        UserService.getUser().then(function (user) {
	          console.log('user: ', user);
	          $scope.user = user;
	        });
	      } else {
	        $scope.user = null;
	      }
    	});

      	$scope.signOut = function(){
      		UserService.logout();
      	}

      }
    };
  }]);
