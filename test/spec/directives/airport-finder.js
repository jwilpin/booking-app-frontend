'use strict';

describe('Directive: airportFinder', function () {

  // load the directive's module
  beforeEach(module('bookingAppFrontendApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<airport-finder></airport-finder>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the airportFinder directive');
  }));
});
